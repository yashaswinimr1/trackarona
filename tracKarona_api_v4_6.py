# -*- coding: utf-8 -*-
"""
Created on Wed May 13 18:28:01 2020

@author: Mohan Raghavan
"""
import importlib

# Get my version number
myname = __file__
ver = myname.split('tracKarona_api_v',2)[1].split('.py',2)[0]
##### 


import numpy as np
from scipy.integrate import odeint, solve_ivp
import matplotlib.pyplot as plt 
import matplotlib.ticker as ticker
import os
from scipy.ndimage.interpolation import shift
import json
import warnings

# Make sure you import only compatible files
# epidemic data from the field
field = importlib.import_module('daily_statewise_'+ver)
# Class for epidemic model
model = importlib.import_module('tracKarona_class_v'+ver)


def define_model_params(in_path,use_model_file):
    # Location for simulator config data 
    pathfile = ''
    # UNCOMMENT NEXT LINE TO REUSE PRE DEFINED PARAMS
    pathfile = in_path+use_model_file
   
    # If config file with parameters already exists
    # ---------------
    if (pathfile != ''):
        print('Using config file '+pathfile)
        with open(pathfile, "r") as infile: 
            stdata = json.load(infile)
            par_list = stdata['par_list']
            ip_params = stdata['ip_params']
            
    else:   
    # No config file exists. Start defining parameters now  
            print(' WARNING!!! MODEL FILE NOT FOUND !!! Using default')
            # model parameters
            # Refer to class_tracKarona for info on parameters
            ip_params= {"beta": 0.5, "fr": 0.03, "frac_c": 0.1, 
                        "frac_iq": 0.1, "inf_t": 9, "lat_t": 7, 
                        "infec_clus_size": 1, "in_phi": 0, "pI": 0.1, 
                        "predict_T": 100, "pre_start_T": -15, 
                        "state": "Vikarabad", "lag_recovery": 5, 
                        "fn_a": "sigmd", "sigmd_lim": 0.01, 
                        "frac_sigmd_start": 2e-05, "frac_sigmd_end": 8e-05
                        }
            par_list = [
                {"par": "in_phi", "time": 28, "ref": "start", "val": 40},
                {"par": "in_phi", "time": 30, "ref": "start", "val": 0}, 
                {"par": "frac_c", "time": 28, "ref": "start", "val": 0.9}
                ]
            # ip_params = {"beta": 0.5777777777777778, "fr": 0.05, 
            #              "frac_c": 0.1, "frac_iq": 0.1, "inf_t": 9, 
            #              "lat_t": 7, "infec_clus_size": 1, "in_phi": 5, 
            #              "pI": 0.01, "predict_T": 10, "pre_start_T": 20, 
            #              "state": "KA", "lag_recovery": 8, "num": 64100000, 
            #              "fn_a": "sigmd", "sigmd_lim": 0.01, 
            #              "frac_sigmd_start": 2e-05, "frac_sigmd_end": 8e-05, 
            #              }
            # # Set of piecewise constant interventions and migrations
            # #par_list is a list of quadruplets
            # # parameter to be modified in intervention
            # # time of intervention 
            # # reference for time: 
            # #    "start" = first day in ref epidemic data
            # #    "today" = last day in reference epidemic data
            # # new value
            # #---
            # # results in a modified list of the param values for 0:T
            # # such that from intervention time onwards until T
            # # the new value comes into force 
            # par_list = [
            #     {"par": "beta", "time": 0, "ref": "start", "val": 5.2/9}, 
            #     {"par": "frac_iq", "time": 0, "ref": "start", "val": 0.1}, 
            #     {"par": "frac_c", "time": 0, "ref": "start", "val": 0.4}, 
            #     {"par": "frac_c", "time": 31, "ref": "start", "val": 0.95},
            #     {"par": "in_phi", "time": 0, "ref": "start", "val": 150}, 
            #     {"par": "in_phi", "time": 14, "ref": "start", "val": 0}
            #     ]
            
    # -------- End of Load / set input parameters
    return ip_params, par_list


def setup_model(ip_params,par_list,in_path,out_path,epid_data_file):
    # State to simulate
    popln = {'MH':114200000, 'DL':21800000,'TN':67900000,
             'RJ':68900000, 'MP':73300000, 'TG':35200000, 'TS':35200000,
             'GJ':62700000, 'UP':204200000, 'AP':49700000, 
             'KL':34800000, 'KA':64100000, 'JK':12500000, 
             'HR':25400000, 'PB':28000000, 'WB':90300000, 
             'BR':99000000, 'GHMC':6810000, 'Suryapet':115000, 
             'Nizamabad':634000, 'Rangareddy':2450000,
             'Medchal':2440000, 'Jogulamba':665000,
             'Warangal Urban':1140000, 'Vikarabad':53143, 
             'None':10000000}
    
    
    # Location where epidemiological data is stored
    daily_data_filename = in_path+epid_data_file

    #-------
    # Adjust times and label settings in ip_params as per 
    # current simulation settings and epidemiological data
    #-----------------
  
    # state
    st_lbl = ip_params['state']
    # Population of the state or cluster
    ip_params['num'] = popln[st_lbl]
         
    # No. of days to simulate before first data point in epidemiological data
    pre_start_T = ip_params['pre_start_T']
    
    # No. of days to simulate post last epidemiological data point
    # i.e. the number of days to predict beyond today
    predict_T = ip_params['predict_T']
    
    # get epidemiological data
    epidem_data = field.get_daily_statewise_data(st_lbl, daily_data_filename)
    # Day in simulation corresponding to today  i.e. last data pt in
    # epidemiological data
    # neg values mean kth from last
    ip_params['sim_today'] = (int)(-1*predict_T)     
    # length of epidemiological data
    epid_data_T = abs(epidem_data[st_lbl]['r_int'][0])
    
    # Total simulation time = length of epidem data + pre + prediction days 
    ip_params['T'] = (int)( predict_T + epid_data_T + pre_start_T)   
    
    
    # Label for output files = statename-data_today
    ip_params['label'] = st_lbl+'-'+str(epidem_data[st_lbl]['today'].date())
    
    #---------------------------
    # Adjust par_list times as per current simulation settings 
    #--------------------------
    new_par_list = []
    for i in par_list:
       ii = {}
       ii['par'] = i['par']
       ii['val'] = i['val']
       ii['ref'] = i['ref']
       if i['ref'] == 'today':
           ii['time'] = (int)(pre_start_T + epid_data_T + i['time'])
       elif i['ref'] == 'start':
           ii['time'] = (int)(pre_start_T + i['time'])
       new_par_list.append(ii)
       

    # Get instance of covid model class
    myclust = model.Kovid_cluster(ip_params,new_par_list,ip_params['T'])
    
    # return the new class instance
    return myclust,epidem_data    


def refine_model(myclust, par_list, epidem_data, tt):

    # No. of days to simulate before first data point in epidemiological data
    pre_start_T = myclust.params['pre_start_T']
    # No. of days to simulate post last epidemiological data point
    predict_T = myclust.params['predict_T']
    # No. of days of epidemic data from field available
    epid_data_T = myclust.T - pre_start_T - predict_T
    
    #---------------------------
    # Adjust par_list times as per current simulation settings 
    #--------------------------
    new_par_list = []
    for i in par_list:
       ii = {}
       ii['par'] = i['par']
       ii['val'] = i['val']
       ii['ref'] = i['ref']
       if i['ref'] == 'today':
           ii['time'] = (int)(pre_start_T + epid_data_T + i['time'])
       elif i['ref'] == 'start':
           ii['time'] = (int)(pre_start_T + i['time'])
       new_par_list.append(ii)
    
    # Update the time varying piece wise continuous functions
    myclust.set_intervention(new_par_list)   

    # Simulate 
    ssivp = solve_ivp(lambda t,x: myclust.dbydt(t,x), [0, myclust.T], 
                       myclust.params['x0'], t_eval = tt, max_step=1) 
    
    # Unpack the results of simulation
    myclust.unpack_odeint_output(ssivp.y.transpose())
        
    
    # Compute error
    s_tr = myclust.params['s_tr']
    N = myclust.params['N']
    data_op = epidem_data[myclust.params['state']]['conf'].to_list()
    model_op_1 = cumIq = np.round(N*s_tr['Iq']) + np.round(N*s_tr['Rq']) + np.round(N*s_tr['Dq'])
    tpre = myclust.params['pre_start_T']
    model_op = model_op_1[(tpre+1):(tpre+1+len(data_op))]
    
    # fig = plt.figure(figsize = (6,4))
    # ax = fig.add_subplot(111)
    # ax.plot(data_op,color='firebrick')
    # ax.plot(model_op,color='lightcoral')
    RMS_err = np.sqrt(np.mean(((model_op-data_op))**2))
    print('RMS Err = '+ str(np.sqrt(np.mean(((model_op-data_op))**2))))
    print('RMS of normalized error = '+str(np.sqrt(np.mean(((model_op-data_op)/data_op)**2))))

    return model_op,data_op,RMS_err

def characterize_model(myclust):    
    # #For iterated plots
    # for i in np.arange(4,6,0.5):
    #     ip_params['beta']=i/9
    #     myclust = setup_data(ip_params,new_par_list,ip_params['T'])
    #     #process_data(myclust,tt,epidem_data[st_lbl])
    #     #sim ODE
    #     #ss = odeint(myclust.dbydt, myclust.params['x0'], tt)
    #     ssivp = solve_ivp(lambda t,x: myclust.dbydt(t,x), [0, ip_params['T']], 
    #                     myclust.params['x0'], t_eval = tt) 
    #     myclust.unpack_odeint_output(ssivp.y.transpose())
    #     myclust.plot(tt,epidem_data[st_lbl],fig, True, 
    #                   varMult=list(['beta', "{:.2f}".format(ip_params['beta'])]))
    print('Model Characterization')
    
def plot_save_model_sim(myclust, ip_params, epidem_data, tt, final_par_list, 
                                                           in_path, out_path):
    # PLot the results
    fig = plt.figure(figsize=(20,12))
    myclust.plot(tt,epidem_data[myclust.params['state']],fig,out_path, False)
    
    #Save configs
    config = {}
    config['ip_params'] = ip_params
    config['par_list'] = final_par_list
   
    pathfile=out_path+myclust.params['label']+'.json'
    with open(pathfile, "w") as outfile: 
        json.dump(config, outfile) 

