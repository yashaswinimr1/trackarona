# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 00:41:36 2020

@author: Mohan Raghavan
"""
import pandas as pd
import matplotlib.pyplot as plt
from scipy.ndimage.interpolation import shift
import numpy as np

def convert_date_to_intervals(date_ar):
    x=np.array([i.date().toordinal() for i in  date_ar])
    dates =[i.date() for i in  date_ar]
    dateIntervals=np.array([(x[i]-x[-1]) for i in range(0,len(x))])
    return dateIntervals

def get_daily_statewise_data(statelist, daily_data_filename):
    
    data = pd.read_csv(daily_data_filename)
    data['Date']= pd.to_datetime(data['Date']) 

    
    statewise = {}
    #state = statelist   
    if (type(statelist) == str):
        statelist = [statelist]
        
    for state in statelist:
        statewise[state] = {}
        dec_data =  data[data['Status']=='Deceased']
        rec_data = data[data['Status']=='Recovered']
        conf_data = data[data['Status']=='Confirmed']
                
        statewise[state]['dec'] = dec_data[state].cumsum()
        statewise[state]['rec'] = rec_data[state].cumsum()
        statewise[state]['conf'] = conf_data[state].cumsum()
        statewise[state]['declog'] = np.log(dec_data[state].cumsum())
        statewise[state]['reclog'] = np.log(rec_data[state].cumsum())
        statewise[state]['conflog'] = np.log(conf_data[state].cumsum())
        
        statewise[state]['r_int'] = convert_date_to_intervals(rec_data['Date'])
        statewise[state]['d_int'] = convert_date_to_intervals(dec_data['Date'])
        statewise[state]['c_int'] = convert_date_to_intervals(conf_data['Date'])
        
        today = rec_data['Date'].iloc[-1]
        firstd = rec_data['Date'].iloc[0]
        if (today != dec_data['Date'].iloc[-1]) or (firstd != dec_data['Date'].iloc[0]) or (len(rec_data) != len(dec_data)):
            raise Exception('Field data for Confirmed, Recovered and deceased are not updated')
        if (today != conf_data['Date'].iloc[-1]) or (firstd != conf_data['Date'].iloc[0]) or (len(rec_data) != len(conf_data)):
            raise Exception('Field data for Confirmed, Recovered and deceased are not updated')
        statewise[state]['today'] = today
        statewise[state]['firstd'] = firstd
        statewise[state]['label'] = state
        return statewise
    
if __name__== "__main__":    

    data = pd.read_csv("statewise_time_series.csv")
    data['Date']= pd.to_datetime(data['Date']) 
    
    dec_data = data[data['Status']=='Deceased']
    rec_data = data[data['Status']=='Recovered']
    conf_data = data[data['Status']=='Confirmed']
    
    plt.style.use('fivethirtyeight')
    fig = plt.figure(figsize=(20,12))
    ax = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    lag = 7
    
    colors = {'TN':'maroon','KL':'red','KA':'coral','TG':'darkorange','AP':'darkgoldenrod','MH':'darkslateblue','GJ':'darkorchid','MP':'royalblue','WB':'purple','RJ':'indigo','UP':'gold','BR':'darkolivegreen','DL':'aqua','PB':'lime','JK':'turquoise'}
    #statelist = ['KA','TG,'UP','PB','WB','KL','RJ','TN','DL','GJ','MP','MH']
    #statelist = ['RJ']
    #statelist = ['TN','KL','KA','TG','AP','MH','GJ','MP','WB','RJ','UP','BR','DL','PB','JK']
    statelist = ['MH']
    #statelist = ['TN','KL','KA','TG','AP']
    #statelist = ['MH','GJ','MP','WB','RJ']
    #statelist = ['UP','BR','DL','PB','JK']
    #alllags = [-15-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
    
    #magnum1 = np.zeros(len(alllags))
    for state in statelist:
            dec = dec_data[state].cumsum()
            rec = rec_data[state].cumsum()
            conf = conf_data[state].cumsum()
            declog = np.log(dec_data[state].cumsum())
            reclog = np.log(rec_data[state].cumsum())
            conflog = np.log(conf_data[state].cumsum())
            
            rdate_int = convert_date_to_intervals(rec_data['Date'])
            ddate_int = convert_date_to_intervals(dec_data['Date'])
            cdate_int = convert_date_to_intervals(conf_data['Date'])
            ax.plot(ddate_int,dec,label=state+'--dec')
            ax.plot(rdate_int,rec,label=state+'--rec')
            ax.plot(cdate_int,conf,label=state+'--conf')
            ax2.plot(ddate_int,declog,color='gray',label=state+'--dec')
            ax2.plot(rdate_int,reclog,color='green',label=state+'--rec')
            ax2.plot(cdate_int,conflog,color='crimson',label=state+'--conf')
            if lag<0:
                 dec_sh = shift(dec,-lag,cval=10000)
                 mag = dec_sh/(dec_sh+rec)
            else:
                rec_sh = shift(rec,-lag,cval=10000)
                mag = dec/(dec+rec_sh)
            #ax2.plot(rdate_int,mag,label=state)
            #print('corrected FR:' + str(np.mean(mag.iloc[-abs(lag)-1-5:-abs(lag)-1])))
                
            
    ax.legend()
    ax2.legend()
    
    #plt.savefig(filename)
    plt.show()
