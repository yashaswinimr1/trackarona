# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 11:02:09 2020

@author: Mohan Raghavan
"""
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.ticker as ticker
from scipy.ndimage.interpolation import shift

# Implementation of modified epidemiological SEIR Model 
#------------------------------------------------
# For details refer preprint: Raghavan et al, 2020
# =============================================================
# Raghavan, M., Sridharan, K. S. and M R, Y. (2020) ‘Using epidemic simulators 
# for monitoring an ongoing epidemic’, medRxiv, p. 2020.05.08.20095463. 
# doi: 10.1101/2020.05.08.20095463.
# =============================================================
# medRxiv 2020.05.08.20095463; doi: https://doi.org/10.1101/2020.05.08.20095463
#------------------------------------------------ 

#

class Kovid_cluster:
    
    # class attribute
    unit = 'cluster'
    myclust={}
    params = {}
    # default parameters for this class
    #---------------------------
    
    #simulation time in days
    #def_T = 400 
    
    #population size
    def_N = 10000
    
    # infection  transmission rate, beta may be calculated directly as
    # beta = (contacts / person) * prob(infec)  
    # or indirectly as beta*gamm OR beta/inf_t 
    ###params['beta'] = [4/9 for i in np.arange(0,T+2,1)] #4/9 
    def_beta = 4/9

    #expectation of latent period (state = Exposed)
    def_lat_t = 7 
    # rate of E --> I transition
    def_k = 1/def_lat_t  
    
    #expectation of infectious period (state = Infectecd)
    def_inf_t = 9 
    #rate of (I --> R) & (Iq-->Rq) transition
    def_gamma = 1/def_inf_t 
    
    # fr fraction of infected that will die 
    def_fr = 0.02 
    #rate (I-->D) & (Iq-->Dq)
    def_delta = def_gamma*def_fr/(1-def_fr) 
    
    #fraction of infected that are detected & isolated
    def_frac_iq = 0.2
      
    #rate of transition I-->Iq
    def_q = (def_gamma+def_delta)*def_frac_iq/(1-def_frac_iq)
     
    #efficiency of contact tracing.. fraction of all contacts traced
    def_frac_c = 0.5
    #rate E-->Eq chosen such that outgo from E is in proportion frac_c
    def_kq = def_k*def_frac_c/(1-def_frac_c)
    #rate S-->Eq chosen such that outgo from S is in proportion frac_c
    def_beta_q = def_beta*def_frac_c/(1-def_frac_c)
    
    #Sz of initial import bringing infection into the population
    def_init_infec_clust_sz = 1
       
    #Population growth in normal non-epidemic times
    def_bounded_popln = 1
    # No births or deaths (other than due to epidemic)
    def_mu = 0 # death rate..
    def_lamda = 0 #birth rate
    
    # Daily inflow/outflow of visitors and migrants from ONE source 
    def_in_phi = 0 # NOTE: absolute numbers of inflow
    def_pI = 0 #probability visitor from source is infected
      
    
    #Constructor
    def __init__(self,ip_params,par_list,T):
   
        # Define all parameters as per input ip_params and par_list
        # ip_params - Dictionary of parameters to initialise the model
        # par_list - List of interpiecewise changes to interventions and
        # incoming migrations
        #
        # Model Compartments
        # -------------------
        # Susceptible(S), Exposed(E), Exposed-Quarantined (Eq)
        # Dead-undetected infection(D), Recovered-undetectefd infection(R)
        # Dead-detected infection(Dq), Recovered-detected infection(Rq)
        # Undetected Infectious(I), Detected Infection + Quarantined (Iq)
        # Setting lamda = mu = 0 => bounded population
        
        # Call model set params
        # Description of parameters in the respective set functions
        self.params['label'] = ip_params['label']
        self.T = ip_params['T']
        self.set_state(ip_params['state'])
        self.set_beta(ip_params['beta'])
        self.set_frac_c(ip_params['frac_c'])
        self.set_frac_iq(ip_params['frac_iq'])
        self.set_fr(ip_params['fr'])
        self.set_inf_t(ip_params['inf_t'])
        self.set_lat_t(ip_params['lat_t'])
        self.set_init_infec_clus_sz(ip_params['infec_clus_size'])
        self.set_N(ip_params['num'])
        self.set_visitor_rates(ip_params['in_phi'], ip_params['pI'])
        self.set_T(ip_params['T'], ip_params['pre_start_T'],
                           ip_params['predict_T'],ip_params['lag_recovery'],
                           ip_params['sim_today'])
        self.set_intervention(par_list)
        self.set_a_params(ip_params['fn_a'],
                                  ip_params['frac_sigmd_start'],
                                  ip_params['frac_sigmd_end'],
                                  ip_params['sigmd_lim'])
        self.set_init_states()
               
        
        self.params['bounded_popln'] = 1
        if self.params['bounded_popln']:
            # No births or deaths (other than due to epidemic)
            self.params['mu'] = 0 # death rate.. keep it same as birth rate so stable population of N
            self.params['lamda'] = 0 #*params['N'] #births as fraction of total population X population
        else:    
            # births and deaths at rate lamda and mu respectively
            self.params['mu'] = 0.00005 # death rate.. keep it same as birth rate so stable population of N
            self.params['lamda'] = 0.00005#*params['N'] #births as fraction of total population X population

        
        
    def set_T(self,T,pre_start_T,predict_T,lag_recovery,sim_today):
        # T = total time to simulate 
        # pre_start_T = time to simulate before epidemic data starts
        # predict_T = number of days of prediction required
        #   = pre_start_T + predict_T + time for which epidemic data available
        self.T = T
        self.params['pre_start_T'] = pre_start_T
        self.params['predict_T'] = predict_T
        self.params['lag_recovery'] = lag_recovery
        # What is the day as per simulation today i.e. last day for which wpidemic
        # data is available
        self.params['sim_today'] = sim_today
    
    def set_N(self,N):    
        # population size
        self.params['N'] = N
        
    def set_beta(self,beta):   
        # set beta - infection  transmission rate,
        #rate S-->Eq/E 
        self.params['beta'] = [beta for i in np.arange(0,self.T+2,1)] 
                      
    def set_lat_t(self,lat_t):  
        # set mean latency period and dependant params k and kq
        
        #expectation of latent period (state = Exposed)
        self.params['lat_t'] = lat_t 
        # rate of E --> I transition
        self.params['k'] = 1/self.params['lat_t']  
                       
    def set_inf_t(self,inf_t): 
        # set mean infectious period 
        #expectation of infectious period (state = Infectecd)
        self.params['inf_t'] = inf_t
        #rate of (I --> R/D) & (Iq-->Rq/D) transition
        self.params['gamma'] = 1/self.params['inf_t'] 
     
    def set_fr(self,fr):    
        # fr fraction of infected that will die 
        self.params['fr'] = fr
                
    def set_frac_iq(self,frac_iq): 
        #fraction of infected that are detected & isolated
        self.params['frac_iq'] = [frac_iq for i in np.arange(0,self.T+2,1)]  
         
    def set_frac_c(self,frac_c):    
        #efficiency of contact tracing.. fraction of all contacts traced
        self.params['frac_c'] = [frac_c for i in np.arange(0,self.T+2,1)] 
                
    def set_init_infec_clus_sz(self,sz):    
        #Sz of initial import bringing infection into the population
        self.params['init_infec_clust_sz'] = sz
        
    def set_bounded_pop(self):    
        self.params['bounded_popln'] = 1
        # No births or deaths (other than due to epidemic)
        self.params['mu'] = 0 # death rate.. keep it same as birth rate so stable population of N
        self.params['lamda'] = 0 #*params['N'] #births as fraction of total population X population
     
    def set_lamda_mu(self,lamda,mu): 
        self.params['bounded_popln'] = 0
        # births and deaths at rate lamda and mu respectively
        self.params['mu'] = mu # death rate.. keep it same as birth rate so stable population of N
        self.params['lamda'] = lamda#*params['N'] #births as fraction of total population X population

    def set_visitor_rates(self,in_phi,pI):
        # Daily inflow/outflow of visitors and migrants from ONE source
        #NOTE: absolute numbers of inflow
        self.params['in_phi'] = [in_phi for i in np.arange(0,self.T+2,1)]
        #probabilty that a random person from source is infected
        self.params['pI'] = [pI for i in np.arange(0,self.T+2,1)] 
    
    def set_state(self,state):
        self.params['state'] = state
        
    def set_a_params(self,fn_type,start,end,lim):
        self.params['fn_a'] = fn_type 
        start = round(start*self.params['N'])
        end = round(end*self.params['N'])
        bwpts = end-start
        self.params['sigmd_cent'] = round(start/2 + end/2)
        self.params['sigmd_slope'] = bwpts*0.5/(np.log(1/lim - 1))
        
    def set_init_states(self):
        # x0 is our initial state (E,I,S,R,D,Iq,Rq,Dq)
        # E,I,Iq are infected
        params = self.params
        x0list =  [0.0 , params['init_infec_clust_sz'], (params['N']-params['init_infec_clust_sz']), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0] # start with 1 infected and all rest susceptible
        x0 = [(1/params['N']) * i for i in x0list]
        params['x0'] = x0
        
    def get_a(self,I):
        a_fn = self.params['fn_a']
        if a_fn == 'sigmd':
            cent = self.params['sigmd_cent']
            slope = self.params['sigmd_slope']
            a = 1- 1/(1+np.exp(-(I-cent)/slope))
        else:
            a = (float)(a_fn) * I/I #Return same shape as input
        return a
    
    # Intervention modeling
    # CAUTION: Only a few are tunable params... ones in brackets are 
    # dependant and automatically changed
    #
    # Frequent: Change with time and intervention measures
    #---------
    # beta -> decreases with social distancing, curfews & lockdown 
    #         increases with pathogen virulence
    # frac_iq(q) -> frac_iq increases with increased testing, detection
    #               and willingness of people to disclose symptoms
    #               and seek medical help       
    # frac_c(kq,beta_q) -> frac_c increases with efficacy of contact tracing and
    #                    quarantining of contacts. When mobility is limited
    #                    quaranting hotspots is a strong form of this intervention
    # in_phi -> decreases with decrease in mobility from source. Sealing of 
    #           borders brings this rate down to zero. Typical numbers are
    #           higher for metros, urban centres with job opportunities
    #           By applying this transitorily, effects of large single source
    #           clusters can be modeled
    # pI -> set according to the proportion of I/E in source population
    # out_phi -> net outgoing visitors to all destinations. decreases with 
    #            curbs on mobility. Typically high in rural and 
    #            unindustrialized areas
    # Moderate: constant for a given pathogen, population or cluster
    #------------
    # lat_t(k), inf_t(gamma), fr(delta) are largely pathogen 
    # specific or may vary with popln genetics
    # they are not expected to change with time. 
    #
    # Population system config - 
    #------------
    # init_infec_clust_sz: number of infected to start with
    # mu, lamda: birth and death rates: determine stable population
    # N: Initial population N. Remains constant if lamda/mu = 1 or both 0
    #
    def set_intervention(self,par_list):
        #par_list is a list of quadruplets
        # parameter to be modified in intervention
        # time of intervention 
        # reference for time: 
        #    "start" = first day in ref epidemic data
        #    "today" = last day in reference epidemic data
        # new value
        #---
        # results in a modified list of the param values for 0:T
        # such that from intervention time onwards until T
        # the new value comes into force 
        
        #convert encoded info above into list[0:T] of param values
        params = self.params
        if any(par_list):
            for entry in par_list:
                print('intervention'+entry['par']+' '+str(entry['time'])+' '+str(entry['val']))
                par = entry['par']
                time = entry['time']
                val = entry['val']
                tmp = params[par] 
                tmp[time:] = [val for i in np.arange(time,len(tmp))]
                params[par] = tmp
                
    def unpack_odeint_output(self,ss):
        #pack state trace into dict
        s_tr = {'E':ss[:,0], 'I':ss[:,1], 'S':ss[:,2], 'R':ss[:,3], 'D':ss[:,4], 'Iq':ss[:,5], 'Rq':ss[:,6], 'Dq':ss[:,7], 'Eq':ss[:,8] }
        self.params['s_tr'] = s_tr
                    
    # tt = time over which to plot
    def plot(self, tt,epidm,fig,out_folder,multFlag,**kwargs):
              
        ##plot results
        params = self.params

        print('#######################   ', params['state'])
        
        # unpack model parameters
        s_tr = self.params['s_tr']
    
        if self.params['sim_today'] < 0:
            tt_neg = tt- self.T - self.params['sim_today']
        else:
            tt_neg = tt - self.params['sim_today']
        N = self.params['N']
        lag_recovery = self.params['lag_recovery']
        beta = params['beta'][0] #infection transmission rate (contacts / per) * (secondary infections / contact) 
        mu = params['mu']  # death rate
        lat_t = params['lat_t'] #expectation of latent period (state = Exposed)
        k = params['k'] #= 1/params['lat_t'] # rate of E --> I transition 
        inf_t = params['inf_t'] #expectation of infectious period (state = Infectecd)
        gamma = params['gamma'] #= 1/params['inf_t'] #rate of I/Iq --> R/Rq transition 
        lamda = params['lamda'] #birth rate 
    
        colors = {'E':'cornflowerblue', 'I':'burlywood', 'S':'forestgreen', 'R':'purple', 'D':'gray'}
        colors_mult = ['cornflowerblue','crimson', 'forestgreen','purple','gray', 'darkorange','gold','violet'] #Changed
        
        plt.style.use('fivethirtyeight')
        plt.interactive(True)
        #fig = plt.figure(figsize=(20,12))
        fig.suptitle(params['state']+'-Today: '+str(epidm['today']))
        ax = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        #ax.plot(tt_neg,N*s_tr['S'],colors['S'],label='Susceptible') #S
        if multFlag:
            #Used only for model characterisation where multiple runs are 
            #overlaid on the same plot
            #print(kwargs['varMult'][0]+str(kwargs['varMult'][1]))
            print('iter::'+str(len(ax.lines)))
            ax.plot(tt_neg,N*s_tr['Iq'] + N*s_tr['I'],colors_mult[len(ax.lines)+1],label=kwargs['varMult'][0]+str(kwargs['varMult'][1])) #Iq
        else:
            ax.plot(tt_neg,np.round(N*s_tr['E']),colors['E'],linestyle=':',label='Exposed undetected') #E
            ax.plot(tt_neg,np.round(N*s_tr['I']),colors['I'],linestyle=':',label='Infected undetected') #I
            #ax.plot(tt_neg,N*s_tr['R'],colors['R'],linestyle='dashed',label='Recovered undetected') #R
            ax.plot(tt_neg,np.round(N*s_tr['D']),color='silver',linestyle=':',label='Dead undetected') #D
            ax.plot(tt_neg,np.round(N*s_tr['Eq']),colors['E'],label='Exposed contact quarantined') #Eq
            ax.plot(tt_neg,np.round(N*s_tr['Iq']),colors['I'],label='Infected quarantined') #Iq
            #ax.plot(tt_neg,N*s_tr['Rq'],colors['R'],label='Recovered post quarantine') #Rq
            ax.plot(tt_neg,np.round(N*s_tr['Dq']),color='silver',label='Dead post quarantine') #Dq
            # print( str(np.shape(epidm['dec']))+'   '+str(np.shape(epidm['conf']))+'   '+str(np.shape(epidm['rec'])))
            cumIq = np.round(N*s_tr['Iq']) + np.round(N*s_tr['Rq']) + np.round(N*s_tr['Dq'])
            ax.plot(tt_neg,cumIq,color='Lightcoral',label='cum(Iq)')
            ax.plot(epidm['c_int'],epidm['conf'],color='firebrick',label=epidm['label']+'-confirmed')
            
            # print( str(np.shape(epidm['dec']))+'   '+str(np.shape(epidm['conf']))+'   '+str(np.shape(epidm['rec'])))
            ax.grid(color='#d4d4d4')
            ax.yaxis.set_major_formatter(ticker.StrMethodFormatter('{x:,.0f}'))
            ax.set_xlabel('Days')
            ax.set_ylabel('# people')
            ax.legend(loc='best', prop={'size':9})
        
        #plot cdf of infection on log scale
        # for cdf use Iq+Rq+Dq+D i.e total infected now or in past (active + recovered) 
        # Only Iq,Rq,Dq are visible
        # I and R go under the radar
        # D invites a postmortem and hence gets detected
        # extract slope of the early increasing curve
        # expected to be a straight line on a log curve
        # Note: Here we use the cdf of Infectons rather than the  
        #       active infections to calculate r0. This is more convenient 
        #       as it is monotonically increasing. Here, still the slope is on account
        #       of active infections and flattening of curve => r0 -> 0 or R0 -> 1  
        nIq = np.round(N*s_tr['Iq'])
        nDq = np.round(N*s_tr['Dq'])
        nRq = np.round(N*s_tr['Rq'])
        shRq = shift(nRq,params['lag_recovery'],cval=0)
        nD =  np.round(N*s_tr['D'])
        nI =  np.round(N*s_tr['I'])
        nR =  np.round(N*s_tr['R'])
        logI = np.log(nIq + nDq + nRq + nD)
        logI[logI<0] = -0.01
        true_logI = np.log(nIq + nDq + nRq + nD + nI + nR)
        true_logI[true_logI<0] = 0
        logRq = np.log(shRq)
        logRq[logRq<0] = -0.01
        logDqD = np.log(nDq + nD)
        logDqD[logDqD<0] = -0.01
        logDq = np.log(nDq)
        # ax2 = fig.add_subplot(222)
        
        if multFlag:
            #print(kwargs['varMult'][0]+str(kwargs['varMult'][1]))
            ax2.plot(tt_neg[0:len(tt)],(true_logI),colors_mult[len(ax2.lines)+1],label=kwargs['varMult'][0]+str(kwargs['varMult'][1]))
        else:
            ax2.plot(tt_neg[0:len(tt)],(logI),color='lightcoral',label='cdf of detected cases')
            
            #ax2.plot(tt_neg[0:len(tt)],(true_logI),color='lightcoral',linestyle=':',label='true cdf of infected')
            ax2.plot(tt_neg[0:len(tt)],logRq,color='mediumaquamarine',label='cdf of recovered')
            ax2.plot(tt_neg[0:len(tt)],(logDqD),color='silver',linestyle=':',label='cdf of deceased(Dq+D) ')
            ax2.plot(tt_neg[0:len(tt)],(logDq),color='silver',label='cdf of detected(Dq) ')
            
            # Plot the real data from field onto same graph
            
            ax2.plot(epidm['c_int'],epidm['conflog'],color='firebrick',label=epidm['label']+'-confirmed')
            ax2.plot(epidm['r_int'],epidm['reclog'],color='green',label=epidm['label']+'-recov')
            ax2.plot(epidm['d_int'],epidm['declog'],color='dimgray',label=epidm['label']+'-dec')
            
        #ax2.plot(tt[0:len(tt)],np.log(N*s_tr['S']),colors['S'],label='unaffected')
        ax2.grid(color='#d4d4d4')
        #ax2.set_xticks(tt_neg[0:len(tt):5])
        ax2.yaxis.set_major_formatter(ticker.StrMethodFormatter('{x:,.2f}'))
        ax2.set_xlabel('Days')
        ax2.set_ylabel('Log # people')
        ax2.legend(loc='best', prop={'size':10})
            
        #plot interventions  with aligned time axis for both graphs
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224)
                        
        for axx in [ax3]:
            Dar = np.array(N*s_tr['D']).round()
            Iar = np.array(N*s_tr['I']).round()
            Iqar = np.array(N*s_tr['Iq']).round()
            Rar = np.array(N*s_tr['R']).round()
            Dqar = np.array(N*s_tr['Dq']).round()
            Rqar = np.array(N*s_tr['Rq']).round()
            shRqar = shift(Rqar,params['lag_recovery'],cval=0)
            #Rqar = shift(Rqar,7,cval=0)
            DqbyDqRq = Dqar/(Dqar + shRqar)
            D_ratio = Dar/(Dar+Dqar)
            first_D = (Dar > 0).argmax() if (Dar > 0).any() else -1
            D_ratio[0:first_D]=0.001
            frac_car = np.array(params['frac_c'])[0:len(tt)]
            frac_iqar = np.array(params['frac_iq'])[0:len(tt)]
            aar = self.get_a(Iar+Iqar)
            betaar = np.array(params['beta'])[0:len(tt)]
            ineff_ind = (1-frac_car)*(aar*(1-frac_car) + (1-aar)*(1-frac_iqar))
            R_eff = ineff_ind*betaar*params['inf_t']
            if multFlag:
                axx.plot(tt_neg[0:len(tt)],DqbyDqRq,label='Dq/(Dq+Rq)-'+kwargs['varMult'][0]+str(kwargs['varMult'][1]))
            else:
                #axx.plot(tt_neg[0:len(tt)],[(1-params['frac_c'][i] )*(1-params['frac_c'][i] )* (1-params['frac_iq'][i]) for i in np.arange(0,len(tt))],label='1-frac_iq . (1-frac_c)^2')
                axx.plot(tt_neg[0:len(tt)],ineff_ind,label='intervention ineffic')
                D_by_D_plus_DQ = np.array([s_tr['D'][i]/(s_tr['D'][i] + s_tr['Dq'][i]) for i in np.arange(0,len(tt))])
                axx.plot(tt_neg[0:len(tt)],D_by_D_plus_DQ,label='D/(Dq+D)')
                axx.plot(tt_neg[0:len(tt)],D_ratio,label='D_ratio')
                # ax2.plot(tt_neg[0:len(tt)],(true_logI),color='lightcoral',linestyle=':',label='true cdf of infected')
                # ax2.plot(tt_neg[0:len(tt)],np.log(nIq+nDq+nRq)-np.log(1-D_by_D_plus_DQ),color='lightcoral',linestyle='dashed',label='true cdf of infected')
                #axx.plot(tt_neg[0:len(tt)],DqbyDqRq,label='Dq/(Dq+Rq)')
                #axx.plot(tt_neg[0:len(tt)],params['fr']*np.ones(len(tt)),label='fr')
                axx.plot(tt_neg[0:len(tt)],0.1*R_eff,color='crimson',label='0.1 X R-eff')
                
                dbydt_log_cdf_I_plus_Iq = np.diff(true_logI,prepend=0)
                
                R_est = 1 + (lat_t + inf_t)*dbydt_log_cdf_I_plus_Iq \
                    + lat_t*inf_t*dbydt_log_cdf_I_plus_Iq*dbydt_log_cdf_I_plus_Iq
                #R_est = 1 + dbydt_log_cdf_I_plus_Iq * inf_t
                R_est[R_est>=inf_t] = inf_t
                axx.plot(tt_neg[0:len(tt)],0.1*R_est,color = 'forestgreen',label='0.1 X R estimated')
                
                # R_est_by_D_ratio = R_est/D_by_D_plus_DQ#D_ratio
                R_est_by_D_ratio = R_est/D_ratio
                R_est_by_D_ratio[R_est_by_D_ratio>inf_t] = inf_t
                # axx.plot(tt_neg[0:len(tt)],R_est_by_D_ratio/inf_t,color='purple',label='beta - estimated')
                
                dbydt_log_cdf_Iq = np.diff(np.log(nIq+nRq+nDq)-np.log(1-D_by_D_plus_DQ),prepend=0)
                R_est_q = 1 + (lat_t + inf_t)*dbydt_log_cdf_Iq \
                    + lat_t*inf_t*dbydt_log_cdf_Iq*dbydt_log_cdf_Iq
                
                # R_est_q_by_D_ratio = R_est_q/D_by_D_plus_DQ#D_ratio
                R_est_q_by_D_ratio = R_est_q/D_ratio
                R_est_q_by_D_ratio[R_est_q_by_D_ratio>inf_t] = inf_t
                
                axx.plot(tt_neg[0:len(tt)],R_est_q_by_D_ratio/inf_t,color='royalblue',label='r_est_q/ D ratio')

            axx.grid(color='#d4d4d4')
            axx.yaxis.set_major_formatter(ticker.StrMethodFormatter('{x:,.2f}'))
            axx.set_xlabel('Days')
            axx.set_ylabel('')
            axx.legend(loc='best', prop={'size':10})
            
        for axx in [ax4]:
            if multFlag:
                axx.plot(tt_neg[0:len(tt)],[100*params[kwargs['varMult'][0]][i] for i in np.arange(0,len(tt))],label=kwargs['varMult'][0]+str(kwargs['varMult'][1]))
            else:
                axx.plot(tt_neg[0:len(tt)],[100*params['beta'][i] for i in np.arange(0,len(tt))],label='100 x beta')
                axx.plot(tt_neg[0:len(tt)],[100*params['frac_iq'][i] for i in np.arange(0,len(tt))],label='q as %')
                axx.plot(tt_neg[0:len(tt)],[100*params['frac_c'][i] for i in np.arange(0,len(tt))],label='c as %')
                axx.plot(tt_neg[0:len(tt)],[params['pI'][i]*params['in_phi'][i] for i in np.arange(0,len(tt))],label='in_phi * pI')
                axx.plot(tt_neg[0:len(tt)],100*aar,label='100x a')
                #log_cdf_Iq = np.log(nIq + nDq + nRq)
                #dbydt_log_cdf_Iq = np.diff(log_cdf_Iq,prepend=0)
                #axx.plot(tt_neg[0:len(tt)],[s_tr['D'][i]/delta for i in np.arange(0,len(tt))],label='Estimated (1-frac_iq)(i-frac_c)' )
                #axx.plot(tt_neg[0:len(tt)],[params['frac_c'][i] * params['frac_iq'][i] for i in np.arange(0,len(tt))],label='1-frac_iq . 1-frac_c')
                axx.plot(tt_neg[0:len(tt)],10*R_eff,color='crimson',linestyle=':',label='10 X R-eff')
            axx.grid(color='#d4d4d4')
            axx.yaxis.set_major_formatter(ticker.StrMethodFormatter('{x:,.0f}'))
            axx.set_xlabel('Days')
            axx.set_ylabel('')
            axx.legend(loc='best', prop={'size':10})
        #find upper end of linear part of log curve assumed to be upto 0.8*max
        loc_upper = (logI > 0.8*max(logI)).argmax() if (logI > 0.8*max(logI)).any() else -1
        #find point where first case is detected
        loc_lower = (logI > 0).argmax() if (logI > 0).any() else -1
        
        #calculate ro as slope on log scale of cdf
        r0 =0
        r0 = (logI[loc_upper]-logI[loc_lower])/(loc_upper-loc_lower)
        print('ro = '+str(r0))
        
        #When was the first case detected
        print('First case detected on day ',str(loc_lower-self.T))
        
        
        #estimated R - the reproductive number from r0 as per Lipsitch et al, 2003
        #This number matches with true R0 only for standard models and constant params
        print('estimated R = '+str(1 + r0*(lat_t+inf_t) + r0*r0*lat_t*inf_t ))
        
        #Prediction for last day of future simulated
        print('Last predicted value-observed: '+str(np.exp(logI[-1])))
        print('Last predicted value-cdf(Iq): '+str(np.exp(np.log(nIq+nRq+nDq)[-1])))
        print('Last predicted value-cdf(I+Iq): '+str(np.exp(true_logI[-1])))
        # print out some derived parameters
        # Use params to find true R0 corresponding to the standard model version
        # This does NOT have any meaning when Standard SEIT model is modified
        if(lamda==0 and mu==0):
            #no birth/death in stable pop, hence lamda/mu = 1 
            R0 = beta*k/(k*gamma)
        else: 
            R0 = (beta*lamda*k/(mu*(mu+k)*(mu+gamma)))
            
        print('R0 =' +str(R0))
        
        # The stable state of susceptible population when in isolation
        if (lamda==0 and mu==0):
            print('Bounded population')
        else:
            print('lamda/mu = '+str(lamda/mu))
        path=out_folder+self.params['label']+'.png'
        print(path)
        plt.savefig(path)
        
        plt.show()


    
    '''------------------------------
    # function: Calculate the gradient 
    ---------------------------------'''
    #def dbydt(self,x, t):
    def dbydt(self, t, x):    
        #Clusterlabel is being passed twice due to some weirdness of odeint
        #doesnt accept when tuple has only 1 element
        #print(type(self))
        #cluster_handle = self.myclust
        #cluster_handle
        #params = cluster_handle.params
        params = self.params
        #Cap t value at T.. sometimes  the integrator steps ahead of T temporarily
        #In such cases use values from time t = T
        #T = cluster_handle.T
        #print('t:'+str(t))#str(int(round(t))))
        
        T = self.T #params['T']
        if t>T:
            t=T
        # unpack state variables    
        #N = self.params['N'] # population size - normalized
        E = x[0]
        I = x[1]
        S = x[2]
        R = x[3]
        D = x[4]
        Iq = x[5]
        Rq = x[6]
        Dq = x[7]
        Eq = x[8]
        
        
        t_idx = int(round(t))
        
        
        # unpack model parameters
        #beta
        #infection transmission rate (contacts / per) * (secondary infections / contact)
        beta = params['beta'][t_idx]  
                 
        # non epidemic death rate
        mu = params['mu'] 
        
        # non epidemic birth rate 
        lamda = params['lamda'] 
        
        #= 1/params['lat_t'] # net rate of E --> I transition
        k = params['k'] 
        
        #= 1/params['inf_t'] #rate of I --> R transition 
        gamma = params['gamma']
        
        # fraction of exposed that are traced-> quarantined -> tested
        # is also the fraction of contact infected that are quarantined
        c = params['frac_c'][t_idx]
        
        # fraction of community infected that are tested and quarantined
        q = params['frac_iq'][t_idx]
        
        # Rate of incoming migrants
        in_phi = params['in_phi'][t_idx]
        
        #Probability of incoming migrants being infected
        pI = params['pI'][t_idx]

        #print ('beta:'+str(beta)+' '+ 'mu:'+str(mu)+' '+'lamda:'+str(lamda)+' ') 
        #print ('gamma:'+str(gamma)+' '+ 'c:'+str(c)+' '+'q:'+str(q)+' ')
        #print ('in_phi:'+str(in_phi)+' '+ 'pI:'+str(pI)+' '+'k:'+str(k)+' ')
        
        #Fraction of infected that acquired it from direct contact
        # as against community spread
        #calculated as a sigmoid(Iq)
        # cent = params['sigmd_cent']
        # slope = params['sigmd_slope']
        # a = 1/(1+np.exp(-(Iq-cent)/slope))
        a = self.get_a(I+Iq)
        #a = 0.5
        
        #fatality rate
        f = params['fr']
        
        #max, min
        mx = 1#1-1e-3
        mn = 0#1e-3
        
        #dE/dt
        dE_dt = beta*S*I*(1-c) - mu*E - k*E + in_phi*0.5*pI/params['N'] 
        
        if (E>mx and dE_dt>0):
             dE_dt = 0
        elif (E<=mn and dE_dt<0):
            dE_dt = 0
             
        
        #dI/dt
        dI_dt = (a*(1-c) + (1-a)*(1-q))*k*E - mu*I - gamma*I + in_phi*0.5*pI/params['N'] 
        if (I>mx and dI_dt>0):
             dI_dt = 0
        elif (I<=mn and dI_dt<0):
            dI_dt = 0
        
        
        #dS/dt
        dS_dt = lamda - mu*S - beta*S*I + in_phi*(1-pI)/params['N']
        if (S>mx and dS_dt>0):
             dS_dt = 0
        elif (S<=mn and dS_dt<0):
            dS_dt = 0
            
        #dR/dt
        dR_dt = gamma*(1-f)*I 
        if (R>mx and dR_dt>0):
             dR_dt = 0
        elif (R<=mn and dR_dt<0):
            dR_dt = 0
            
        #dD/dt
        dD_dt = gamma*f*I 
        if (D>mx and dD_dt>0):
             dD_dt = 0
        elif (D<=mn and dD_dt<0):
            dD_dt = 0
            
        #dIq/dt
        dIq_dt = (a*c+ (1-a)*q)*k*E + k*Eq - gamma*Iq 
        if (Iq>mx and dIq_dt>0):
             dIq_dt = 0
        elif (Iq<=mn and dIq_dt<0):
            dIq_dt = 0
            
        #dRq/dt
        dRq_dt = gamma*(1-f)*Iq 
        if (Rq>mx and dRq_dt>0):
             dRq_dt = 0
        elif (Rq<=mn and dRq_dt<0):
            dRq_dt = 0
        
        #dDq/dt
        dDq_dt = gamma*f*Iq 
        if (Dq>mx and dDq_dt>0):
             dDq_dt = 0
        elif (Dq<=mn and dDq_dt<0):
            dDq_dt = 0
      
        #dEq/dt
        dEq_dt = beta*c*S*I - k*Eq 
        if (Dq>mx and dDq_dt>0):
             dDq_dt = 0
        elif (Dq<=mn and dDq_dt<0):
            dDq_dt = 0
       
                   
        #load and return
        dx_dt = [dE_dt, dI_dt, dS_dt, dR_dt, dD_dt, dIq_dt, dRq_dt, dDq_dt, dEq_dt]
        return dx_dt
