# -*- coding: utf-8 -*-
"""
Created on Wed May 13 22:21:33 2020


@author: Mohan Raghavan


"""
import importlib

# Get my version number
myname = __file__
ver = myname.split('tracKarona_eg_v',2)[1].split('.py',2)[0]
##### 


import numpy as np
import os
import warnings

# Make sure you import only compatible files
# epidemic data from the field
field = importlib.import_module('daily_statewise_'+ver)
# Class for api
tracKarona = importlib.import_module('tracKarona_api_v'+ver)


    
'''------------------------------
# Start main
---------------------------------'''    
if __name__== "__main__":
    
    #Turn off warnings
    warnings.filterwarnings("ignore")

    # Input and output folders
    out_path = os.getcwd()+'/out/'
    in_path = os.getcwd()+'/in/'
    # Use pre-defined configs that uses data ONLY upto this date for modeling
    # use_model_file = 'Suryapet-2020-05-11.json'
    # State codes
    # 'MH', 'DL','TN','RJ','MP', 'TG','GJ', 'UP',  
    # 'KL', 'KA', 'PB', 'WB' 
    use_model_file = 'KA-2020-04-17.json'

    # Nevertheless display and use the epidemic data from field upto this date 
    epid_data_file = 'state_wise_daily_2020-04-17.csv'
   
    # Setup params from scratch or load pre stored configs
    ip_params, par_list = tracKarona.define_model_params(in_path, use_model_file)
 
    # Instantiate class and load corresponding reference data from the field 
    myclust,epidem_data = tracKarona.setup_model(ip_params, par_list, in_path, out_path, 
                                          epid_data_file)
    tt = np.arange(0, myclust.T, 1) # range times to simulate
    
    # Do the following until error is acceptable
    #   1. Refine model model and generate error estimate
    #   2. Modify the par_list and check error with new params
    # 
    acceptable_err = False
    cnt = 0        
    while (acceptable_err == False):
        # model output, epidemic data output and RMS error between them
        model_op, data_op, RMS_err = tracKarona.refine_model(myclust, par_list,
                                                             epidem_data,tt)
        ### % % % % % % % % % % %
        
        # CALL FUNC HERE 
        # for e.g. 
        #   par_list = get_next_param_iter(par_list, model_op, data_op, RMS_err)
        # to learn better par_list for next iteration 
        #par_list = refined par_list
        
        ### % % % % % % % % % % %
        # this is a dummy to execute loop 3 times
        # Instead use a condition like "if RMS_err < 10"
        cnt = cnt + 1
        if cnt == 1:
            acceptable_err = True
    
    # PLot and save the outputs of model simulation with final params
    tracKarona.plot_save_model_sim(myclust, ip_params, epidem_data, tt, 
                                   par_list, in_path, out_path)
    
    # Done  