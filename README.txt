#####
# tracKarona v4.6
#####
# Implementation of modified epidemiological SEIR Model 
#------------------------------------------------
# For details refer preprint: Raghavan et al, 2020
# =============================================================
# Raghavan, M., Sridharan, K. S. and M R, Y. (2020) ‘Using epidemic simulators 
# for monitoring an ongoing epidemic’, medRxiv, p. 2020.05.08.20095463. 
# doi: 10.1101/2020.05.08.20095463.
# =============================================================
# medRxiv 2020.05.08.20095463; doi: https://doi.org/10.1101/2020.05.08.20095463
#------------------------------------------------ 
# 
# correspondence: mohanr [at] bme.iith.ac.in
#
# This folder contains the epidemic simulator tracKarona, 
# specifically for the tracking the COVID-19 pandemic
# 
# Contents
# --------
# 1. tracKarona_class_v4.6  :   The main class that implements the
#                               modified SEIRD model as in Raghavan, 2020
# 2. tracKarona_api_v4.6    :   Implements the apis that need to be called
#                               in order to use the simulator
# 3. tracKarona_eg_v4.6     :   Is an example for representative purposes
#                               You can change the state to be simulated
#                               by using appropriate state code in defn of
#                               use_model_file
#               IMPORTANT   :   If you plan to implement any learning
#                               algorithms for automated parameterization, 
#                               GOTO Lines 68 onwards or lookout for 
#                               "CALL FUNC HERE"
# 4. daily_statewise_v4.6   :   Helper function that reads in epidemic data
#                               from xl sheets.
# 5. in                     :   Folder containing input files
#
# 6. out                    :   Folder containing output files
#
# 